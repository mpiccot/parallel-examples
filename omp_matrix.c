#include <omp.h> 
#include <stdio.h> 
#include <stdlib.h> 
#define N 512 
 
int main(int argc, char *argv) { 
 omp_set_num_threads(16);//set number of threads here 
 int i, j, k, m; 
 double sum; 
 double start, end, total = 0; // used for timing  
 double A[N][N], B[N][N], C[N][N]; 
 
 for (i = 0; i < N; i++) { 
 for (j = 0; j < N; j++) { 
 A[i][j] = j*1; 
 B[i][j] = i*j+2; 
 C[i][j] = j-i*2; 
 } 
 }
for (m = 0; m < 10; m++){ 
 start = omp_get_wtime(); //start time measurement
// #pragma omp parallel
// {
// #pragma omp for
 for (i = 0; i < N; i++) {
  
 for (j = 0; j < N; j++) { 
 sum = 0;
 for (k=0; k < N; k++) { 
 sum += A[i][k]*B[k][j]; 
 } 
 C[i][j] = sum; 
 } 
 } 
// }
 end = omp_get_wtime(); //end time measurement
 total += (end-start); 
 printf("Time of computation: %f seconds\n", end-start);
}
 printf("Average time: %f seconds\n", (total/10.0)); 
 return(0); 
} 