#include <stdio.h> 
#include <string.h> 
#include <stddef.h> 
#include <stdlib.h> 
#include "mpi.h"
#include <math.h>

main(int argc, char **argv)
{
 int rank, size;
 MPI_Status status; 
 MPI_Init(&argc, &argv); 
 MPI_Comm_size(MPI_COMM_WORLD, &size); 
 MPI_Comm_rank(MPI_COMM_WORLD, &rank);
 double elapsed_time, start_time, end_time;
 struct timeval tv1, tv2;  
 double x[2], y, est, dist;
 int i, inside = 0;
 int samp = 10000, open, kill = 0;
 //get random seed based on time
 srand(time(NULL));
gettimeofday(&tv1, NULL);
 //loop 10k times
if (rank == 0){
//first processes hardcoded
for (i = 1; i < size; i++){
x[0] = (double)rand()/RAND_MAX;
x[1] = (double)rand()/RAND_MAX;
MPI_Send(&x, 2, MPI_DOUBLE, 1, 99, MPI_COMM_WORLD);
}
 for (i=(size-1); i < samp; i++){
  //compute new x,y
  x[0] = (double)rand()/RAND_MAX;
  x[1] = (double)rand()/RAND_MAX;
  //wait for a process to be ready, and check which process it is
  MPI_Recv(&open, 1, MPI_INT, MPI_ANY_SOURCE, 99, MPI_COMM_WORLD, &status);
  //recieve from that process first
  MPI_Recv(&dist, 1, MPI_DOUBLE, open, 99, MPI_COMM_WORLD, &status);
  //then send the new computed x,y
  MPI_Send(&x, 2, MPI_DOUBLE, open, 99, MPI_COMM_WORLD);
  
  //compute dist
  if (dist <= 1.0) 
   inside++;}
 //get estimate
 est = (double)inside/samp*4;
 gettimeofday(&tv2, NULL); 
 
 elapsed_time = (tv2.tv_sec - tv1.tv_sec) + 
 ((tv2.tv_usec - tv1.tv_usec) / 1000000.0); 
 printf ("elapsed_time=\t%lf (seconds)\n", elapsed_time);
 printf("estimate is %f\n",est);
 //broadcast a kill switch when done
 kill = 1;
 MPI_Bcast (&kill,1,MPI_INT,0,MPI_COMM_WORLD);
}
else{
//first recieves hardcoded
MPI_Recv(&x, 2, MPI_DOUBLE, 0, 99, MPI_COMM_WORLD, &status);
while (kill == 0){
  dist = (x[0] * x[0]) + (x[1] * x[1]);
  //set open to rank so the master knows which process is ready
  open = rank;
  //send that you are ready
  MPI_Send(&open, 1, MPI_INT, 0, 99, MPI_COMM_WORLD);
  //send your dist after you have stated you are ready
  MPI_Send(&dist, 1, MPI_DOUBLE, 0, 99, MPI_COMM_WORLD);
  //then wait for your next x,y
  MPI_Recv(&x, 2, MPI_DOUBLE, 0, 99, MPI_COMM_WORLD, &status);
}
}
 MPI_Finalize();
}