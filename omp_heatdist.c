#include <omp.h> 
#include <stdio.h> 
#include <stdlib.h>
//valid n value examples: 10, 50, 90, 130, 170... (see below for more info)
#define n 290
#define limit 10000

int main(int argc, char *argv) { 
 //omp_set_num_threads(16);
 double h[2][n][n] = {0};
 int i, j, iteration, loc;
 int test1, test2;
 double start, end;
 //set up boundries
for (j = 0; j < 2; j++){
 for (i = 0; i < n; i++){
  h[j][0][i] = 20;
  h[j][n-1][i] = 20;
  h[j][i][0] = 20;
  h[j][i][n-1] = 20;
}
 for (i = (n*0.3); i < ((n*0.3)+(n*0.4)); i++){
  h[j][0][i] = 100;
}}
start = omp_get_wtime();
 //end set up boundries
//loop for iterations
 for (iteration = 0; iteration < limit; iteration++) {
 //for figuring out which 3rd dim of matrix to use
 loc = iteration%2;
 #pragma omp parallel for private(j)
//calculate all except for outside square 
 for (i = 1; i < (n-1); i++){
 for (j = 1; j < (n-1); j++){ 
 h[((loc*-1)+1)][i][j] = 0.25 * (h[loc][i-1][j] + h[loc][i+1][j] + h[loc][i][j-1] + h[loc][i][j+1]); 
 }}}
end = omp_get_wtime();
//temp var so not calculated every for loop
int var = ((n-2)*.125);
//output 8 values, start at the first first value off the wall at
//top left and go for 8 equal values (see note for output explained)
for (test1 = 0; test1 < 8; test1++){
for (test2 = 0; test2 < 8; test2++){
printf("%f ",h[((loc*-1)+1)][(test1*var)+1][(test2*var)+1]);
}
printf("\n");
}
printf("Execution time: %f",(end-start));
return(0);
}