#define N 512 
#include <stdio.h>
#include <stdlib.h> 
#include <math.h> 
#include <sys/time.h>
#include "mpi.h" 
 
print_results(char *prompt, double a[N][N]); 
 
int main(int argc, char *argv[]) 
{ 
 int i, j, k, blksz, error = 0; 
 double a[N][N], b[N][N], c[N][N] = {0};
 char *usage = "Usage: %s file\n"; 
 FILE *fd; 
 double elapsed_time, start_time, end_time; 
 struct timeval tv1, tv2;
 //ADDED: MPI setup
 int size, rank;
 MPI_Status status; 
 
 MPI_Init(&argc, &argv); 
 MPI_Comm_size(MPI_COMM_WORLD, &size); 
 MPI_Comm_rank(MPI_COMM_WORLD, &rank);
 blksz = (int) ceil ((double) N/size);
 double d[N][N], e[N][N] = {0};
//END ADDED
if (argc< 2) { 
 fprintf (stderr, usage, argv[0]); 
 error = -1; 
 } 
 
 if ((fd = fopen (argv[1], "r")) == NULL) { 
 //ADDED: Only need to ouput that we can't open the file once, on rank 0
 if (rank == 0){
  fprintf (stderr, "%s: Cannot open file %s for reading.\n", 
  argv[0], argv[1]); 
  fprintf (stderr, usage, argv[0]);
  }
//END ADDED 
 error = -1; 
 }
 //If an error is returned, finalize and exit, since all processes check for files, no need to bcast
 //NOTE: I'm not sure what would happen if one process found the file and another did not (and not sure how that situation could ever happen either). Probably an error about exiting before finalizing.
 if (error != 0){
  MPI_Finalize();
  exit(0);
}
//Debugging barrier
MPI_Barrier(MPI_COMM_WORLD);
//Only the master process needs to read. This was initially done by every process, but it would make scattering and broadcasting redundant if left alone, so I encased it in rank == 0
if (rank == 0){
 for (i = 0; i< N; i++) 
 for (j = 0; j < N; j++) 
 fscanf (fd, "%lf", &a[i][j]); 
 
 for (i = 0; i< N; i++) 
 for (j = 0; j < N; j++) 
 fscanf (fd, "%lf", &b[i][j]);}
 //Barrier before timestamp to get correct time up to now 
 MPI_Barrier(MPI_COMM_WORLD);
 // Take a time stamp 
 gettimeofday(&tv1, NULL);
 //Set block size
 

 //Scatter info for each block of to mini matrix d

 MPI_Scatter (a,blksz*N,MPI_DOUBLE,d,blksz*N,MPI_DOUBLE,0,MPI_COMM_WORLD);
 
 //Broadcast b to all other processes

 MPI_Bcast (b,N*N,MPI_DOUBLE,0,MPI_COMM_WORLD);

 //Compute info, only need to compute rows equal to block size, store in e mini matrix for each process
 for (i = 0; i < blksz; i++){
  for (j = 0; j < N; j++){
   for (k = 0; k < N; k++){
    e[i][j] += d[i][k] * b[k][j];}}}
 //Gather info from e from each process and store in c
 MPI_Gather(e,blksz*N,MPI_DOUBLE,c,blksz*N,MPI_DOUBLE,0,MPI_COMM_WORLD);

 // Take a time stamp. This won't happen until after the master 
 // process has gathered all the input from the other processes.
 //Only the master process needs to do this
if (rank == 0){ 
 gettimeofday(&tv2, NULL); 
 
 elapsed_time = (tv2.tv_sec - tv1.tv_sec) + 
 ((tv2.tv_usec - tv1.tv_usec) / 1000000.0); 
 printf ("elapsed_time=\t%lf (seconds)\n", elapsed_time); 
 }
 // print results ONLY if rank is 0
 if (rank == 0){
  print_results("C = ", c);}
 MPI_Finalize();
} 
 
print_results(char *prompt, double a[N][N]) 
{ 
 int i, j; 
 
 printf ("\n\n%s\n", prompt); 
 
 for (i = 0; i< N; i++) { 
 for (j = 0; j < N; j++) { 
 printf(" %.2lf", a[i][j]); 
 } 
 printf ("\n"); 
 } 
 printf ("\n\n"); 
}